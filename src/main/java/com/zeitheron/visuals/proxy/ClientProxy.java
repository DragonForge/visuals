package com.zeitheron.visuals.proxy;

import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_DOUBLE_SAW;
import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_ENDER_INSIDE_TRANSPARENT;
import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_ENDER_SAW;
import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_SINGLE_SAW;
import static com.zeitheron.visuals.client.tex.TextureTransformer.transform;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import com.zeitheron.hammercore.client.render.vertex.SimpleBlockRendering;
import com.zeitheron.hammercore.client.utils.ItemColorHelper;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.BufferedTexture;
import com.zeitheron.hammercore.client.utils.texture.TexLocUploader;
import com.zeitheron.hammercore.event.client.EnderInventoryAcceptEvent;
import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;
import com.zeitheron.hammercore.lib.zlib.utils.IndexedMap;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.hammercore.utils.FinalFieldHelper;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.visuals.Visuals;
import com.zeitheron.visuals.api.client.WLAR;
import com.zeitheron.visuals.client.GLDownloader;
import com.zeitheron.visuals.client.particle.FXSparkle;
import com.zeitheron.visuals.client.tesr.TESRChestModified;
import com.zeitheron.visuals.client.tesr.TESREnderChestModified;
import com.zeitheron.visuals.client.tesr.TESRJukebox;
import com.zeitheron.visuals.client.tex.TextureTransformer;
import com.zeitheron.visuals.client.wlar.WLARAnvil;
import com.zeitheron.visuals.client.wlar.WLARCraftingTable;
import com.zeitheron.visuals.client.wlar.WLAREnchantmentTable;
import com.zeitheron.visuals.config.VConfigClient;
import com.zeitheron.visuals.init.BlocksV;
import com.zeitheron.visuals.tiles.TileEntityJukeboxFixed;
import com.zeitheron.visuals.util.CallableMap;
import com.zeitheron.visuals.util.TopStackHelper;

import io.netty.util.internal.ThreadLocalRandom;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityEnchantmentTable;
import net.minecraft.tileentity.TileEntityEnderChest;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldEventListener;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class ClientProxy extends CommonProxy implements IWorldEventListener
{
	public static NonNullList<ItemStack> enderChestTop8Items = NonNullList.withSize(8, ItemStack.EMPTY);
	public static final IndexedMap<BlockPos, TwoTuple.Atomic<Float, Boolean>> lamps = new IndexedMap<>();
	public static final IndexedMap<BlockPos, NonNullList<ItemStack>> renderData = new IndexedMap<>();
	
	public static int lampsWidth;
	
	@Override
	public ResourceLocation trimToHead(ResourceLocation skin)
	{
		TextureManager mgr = Minecraft.getMinecraft().getTextureManager();
		ResourceLocation trim = new ResourceLocation(skin.getNamespace(), skin.getPath() + "_visuals_trimmed.png");
		
		ITextureObject def;
		if(mgr.getTexture(trim) == null && (def = mgr.getTexture(skin)) != null)
		{
			BufferedImage origin = GLDownloader.toBufferedImage(def.getGlTextureId());
			
			int w = origin.getWidth();
			
			BufferedImage target = new BufferedImage(w / 2, w / 2, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = target.createGraphics();
			g.drawImage(origin.getSubimage(0, 0, w / 2, w / 4), 0, 0, null);
			g.drawImage(origin.getSubimage(w / 2, 0, w / 2, w / 4), 0, 0, null);
			g.dispose();
			
			mgr.loadTexture(trim, new BufferedTexture(target));
			TexLocUploader.cleanupAfterLogoff(trim);
			
			Visuals.LOG.info("TRIMMED \"" + skin + "\" -> \"" + trim + "\"");
		}
		
		return trim;
	}
	
	@Override
	public void init()
	{
		int leftoverColor = 0x4F1F00;
		
		if(BlocksV.ORGANIC_LEFTOVERS != null)
		{
			Minecraft.getMinecraft().getItemColors().registerItemColorHandler((stack, tint) -> leftoverColor, BlocksV.ORGANIC_LEFTOVERS);
			Minecraft.getMinecraft().getBlockColors().registerBlockColorHandler((state, world, pos, tint) -> leftoverColor, BlocksV.ORGANIC_LEFTOVERS);
		}
		
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityChest.class, new TESRChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityEnderChest.class, new TESREnderChestModified());
		if(VConfigClient.animations_jukebox)
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityJukeboxFixed.class, new TESRJukebox());
		
		try
		{
			TextureManager tex = Minecraft.getMinecraft().getTextureManager();
			Field f = TextureManager.class.getDeclaredFields()[2];
			Map<ResourceLocation, ITextureObject> newMap = new CallableMap<ResourceLocation, ITextureObject>(tex.mapTextureObjects, (res, tobj) ->
			{
				for(Predicate<ResourceLocation> pred : TextureTransformer.texturePredicates)
					if(pred.test(res))
					{
						TextureTransformer.transform(res, TextureTransformer.processorsPredicates.get(pred));
						return tobj;
					}
				return tobj;
			});
			FinalFieldHelper.setFinalField(f, tex, newMap);
		} catch(ReflectiveOperationException e1)
		{
			e1.printStackTrace();
		}
		
		WLAR.register(TileEntityEnchantmentTable.class, new WLAREnchantmentTable());
		WLAR.register(Blocks.ANVIL, new WLARAnvil());
		WLAR.register(Blocks.CRAFTING_TABLE, new WLARCraftingTable());
		
		// Vanilla
		transform(new ResourceLocation("minecraft", "textures/entity/chest/christmas.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/normal.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/trapped.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/christmas_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/normal_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/trapped_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/ender.png"), CHEST_ENDER_SAW);
		
		// Visuals
		transform(new ResourceLocation("visuals", "textures/entity/chest/ender.png"), CHEST_ENDER_INSIDE_TRANSPARENT);
		transform(new ResourceLocation("visuals", "textures/builtin/lamp.png"), nul ->
		{
			BufferedImage atlas = GLDownloader.toBufferedImage(Minecraft.getMinecraft().getTextureManager().getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).getGlTextureId());
			TextureAtlasSprite onTex = Minecraft.getMinecraft().getBlockRendererDispatcher().getModelForState(Blocks.LIT_REDSTONE_LAMP.getDefaultState()).getParticleTexture();
			TextureAtlasSprite offTex = Minecraft.getMinecraft().getBlockRendererDispatcher().getModelForState(Blocks.REDSTONE_LAMP.getDefaultState()).getParticleTexture();
			BufferedImage off = GLDownloader.exportSprite(offTex, atlas);
			BufferedImage on = GLDownloader.exportSprite(onTex, atlas);
			int msw = lampsWidth = Math.min(on.getWidth(), off.getWidth());
			{
				BufferedImage temp = new BufferedImage(msw, msw, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = temp.createGraphics();
				g.drawImage(off, 0, 0, msw, msw, null);
				g.dispose();
				off = temp;
				
				temp = new BufferedImage(msw, msw, BufferedImage.TYPE_INT_ARGB);
				g = temp.createGraphics();
				g.drawImage(on, 0, 0, msw, msw, null);
				g.dispose();
				on = temp;
			}
			BufferedImage nat = new BufferedImage(msw * 10, msw * 10, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = nat.createGraphics();
			for(int atx = 0; atx < 10; ++atx)
				for(int aty = 0; aty < 10; ++aty)
				{
					int cur = atx + aty * 10;
					float progress = cur / 100F;
					
					BufferedImage res = new BufferedImage(msw, msw, BufferedImage.TYPE_INT_ARGB);
					
					for(int x = 0; x < msw; ++x)
						for(int y = 0; y < msw; ++y)
							res.setRGB(x, y, ColorHelper.interpolate(off.getRGB(x, y), on.getRGB(x, y), progress));
						
					g.drawImage(res, atx * msw, aty * msw, null);
				}
			g.dispose();
			return nat;
		});
	}
	
	@SubscribeEvent
	public final void tick(ClientTickEvent e)
	{
		EntityPlayerSP player = Minecraft.getMinecraft().player;
		
		if(player != null && player.ticksExisted % 10 == 0)
			enderChestTop8Items = TopStackHelper.getTopItems(player.getInventoryEnderChest(), 8);
		
		TextureTransformer.update();
		
		if(e.phase == Phase.END && player != null && !Minecraft.getMinecraft().isGamePaused())
		{
			List<BlockPos> positions = renderData.getKeys();
			for(int i = 0; i < positions.size(); ++i)
			{
				BlockPos pos = positions.get(i);
				double dist = Math.sqrt(player.getDistanceSqToCenter(pos));
				if(dist > 128)
					renderData.remove(pos);
				else
				{
					World world = player.world;
					NonNullList<ItemStack> list = renderData.getValues().get(i);
					
					IBlockState state = world.getBlockState(pos);
					TileEntity tile = world.getTileEntity(pos);
					
					if(state.getBlock() == Blocks.AIR)
						renderData.remove(pos);
					
					if(tile instanceof TileEntityEnchantmentTable && list.size() == 2)
					{
						ItemStack toEnch = list.get(0);
						ItemStack lapis = list.get(1);
						
						if(!toEnch.isEmpty() && !lapis.isEmpty())
						{
							ThreadLocalRandom rng = ThreadLocalRandom.current();
							
							if(rng.nextInt(2) == 0)
							{
								FXSparkle fx = new FXSparkle(world, pos.getX() + .5, pos.getY() + .9, pos.getZ() + .5, (rng.nextFloat() - rng.nextFloat()) * .05F, .1, (rng.nextFloat() - rng.nextFloat()) * .05F, ItemColorHelper.DEFAULT_GLINT_COLOR, 100);
								fx.setGravity(.1F);
								ParticleProxy_Client.queueParticleSpawn(fx);
							}
						}
					}
				}
			}
			
			positions = lamps.getKeys();
			for(int i = 0; i < positions.size(); ++i)
			{
				BlockPos pos = positions.get(i);
				TwoTuple.Atomic<Float, Boolean> list = lamps.getValues().get(i);
				
				boolean add = list.get2();
				
				list.set1(Math.max(0, Math.min(1, list.get1() + (add ? .025F : -.025F))));
				
				if(add ? list.get1() >= 1F : list.get1() <= 0F)
					lamps.remove(pos);
			}
		}
	}
	
	private static final TextureAtlasSprite dummySprite = new TextureAtlasSprite("")
	{
	};
	
	@SubscribeEvent
	public final void renderWorld(RenderWorldLastEvent e)
	{
		World world = Minecraft.getMinecraft().world;
		if(world == null)
			return;
		
		float time = e.getPartialTicks();
		
		Minecraft.getMinecraft().entityRenderer.enableLightmap();
		
		GlStateManager.disableCull();
		GlStateManager.enableBlend();
		GlStateManager.enableFog();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		
		List<BlockPos> positions = renderData.getKeys();
		for(int i = 0; i < positions.size(); ++i)
		{
			BlockPos pos = positions.get(i);
			NonNullList<ItemStack> list = renderData.getValues().get(i);
			
			IBlockState state = world.getBlockState(pos);
			TileEntity tile = world.getTileEntity(pos);
			
			WLAR.render(world, tile, state, pos, pos.getX() - TileEntityRendererDispatcher.staticPlayerX, pos.getY() - TileEntityRendererDispatcher.staticPlayerY, pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ, time, list);
		}
		
		GlStateManager.enableCull();
		GlStateManager.disableBlend();
		GlStateManager.disableFog();
		
		Minecraft.getMinecraft().entityRenderer.disableLightmap();
		
		//
		
		Minecraft.getMinecraft().entityRenderer.enableLightmap();
		
		GlStateManager.disableCull();
		GlStateManager.enableBlend();
		GlStateManager.enableFog();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		
		SimpleBlockRendering sbr = RenderBlocks.getInstance().simpleRenderer;
		
		dummySprite.setIconWidth(lampsWidth);
		dummySprite.setIconHeight(lampsWidth);
		
		UtilsFX.bindTexture("visuals", "textures/builtin/lamp.png");
		
		positions = lamps.getKeys();
		for(int i = 0; i < positions.size(); ++i)
		{
			BlockPos pos = positions.get(i);
			TwoTuple.Atomic<Float, Boolean> list = lamps.getValues().get(i);
			
			boolean add = list.get2();
			
			if(add ? list.get1() >= 1F : list.get1() <= 0F)
				continue;
			
			float progress = Math.max(0, Math.min(1, list.get1() + time * (add ? .025F : -.025F)));
			int tex = (int) (progress * 100);
			
			dummySprite.initSprite(dummySprite.getIconWidth() * 10, dummySprite.getIconWidth() * 10, dummySprite.getIconWidth() * (tex % 10), dummySprite.getIconWidth() * (tex / 10), false);
			
			int l = sbr.rb.setLighting(world, pos);
			sbr.begin();
			sbr.setRenderBounds(Block.FULL_BLOCK_AABB.grow(.001));
			sbr.setSprite(dummySprite);
			sbr.setBrightness(l);
			sbr.drawBlock(pos.getX() - TileEntityRendererDispatcher.staticPlayerX, pos.getY() - TileEntityRendererDispatcher.staticPlayerY, pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ);
			Tessellator.getInstance().draw();
		}
		
		GlStateManager.enableCull();
		GlStateManager.disableBlend();
		GlStateManager.disableFog();
		
		Minecraft.getMinecraft().entityRenderer.disableLightmap();
	}
	
	@SubscribeEvent
	public final void worldLoad(WorldEvent.Load e)
	{
		if(e.getWorld() instanceof WorldClient)
			e.getWorld().addEventListener(this);
	}
	
	@Override
	public void notifyBlockUpdate(World worldIn, BlockPos pos, IBlockState oldState, IBlockState newState, int flags)
	{
		if(!worldIn.isRemote)
			return;
		if(oldState.getBlock() == Blocks.REDSTONE_LAMP && newState.getBlock() == Blocks.LIT_REDSTONE_LAMP)
		{
			TwoTuple.Atomic<Float, Boolean> p = lamps.get(pos);
			if(p == null)
				lamps.put(pos, new TwoTuple.Atomic<Float, Boolean>(0F, true));
			else
				p.set2(true);
		} else if(oldState.getBlock() == Blocks.LIT_REDSTONE_LAMP && newState.getBlock() == Blocks.REDSTONE_LAMP)
		{
			TwoTuple.Atomic<Float, Boolean> p = lamps.get(pos);
			if(p == null)
				lamps.put(pos, new TwoTuple.Atomic<Float, Boolean>(1F, false));
			else
				p.set2(false);
		} else if(oldState.getBlock() == Blocks.REDSTONE_LAMP || oldState.getBlock() == Blocks.LIT_REDSTONE_LAMP)
			lamps.remove(pos);
	}
	
	@Override
	public void notifyLightSet(BlockPos pos)
	{
	}
	
	@Override
	public void markBlockRangeForRenderUpdate(int x1, int y1, int z1, int x2, int y2, int z2)
	{
	}
	
	@Override
	public void playSoundToAllNearExcept(EntityPlayer player, SoundEvent soundIn, SoundCategory category, double x, double y, double z, float volume, float pitch)
	{
	}
	
	@Override
	public void playRecord(SoundEvent soundIn, BlockPos pos)
	{
	}
	
	@Override
	public void spawnParticle(int particleID, boolean ignoreRange, double xCoord, double yCoord, double zCoord, double xSpeed, double ySpeed, double zSpeed, int... parameters)
	{
	}
	
	@Override
	public void spawnParticle(int id, boolean ignoreRange, boolean minimiseParticleLevel, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed, int... parameters)
	{
	}
	
	@Override
	public void onEntityAdded(Entity entityIn)
	{
	}
	
	@Override
	public void onEntityRemoved(Entity entityIn)
	{
	}
	
	@Override
	public void broadcastSound(int soundID, BlockPos pos, int data)
	{
	}
	
	@Override
	public void playEvent(EntityPlayer player, int type, BlockPos blockPosIn, int data)
	{
	}
	
	@Override
	public void sendBlockBreakProgress(int breakerId, BlockPos pos, int progress)
	{
	}
	
	@SubscribeEvent
	public void acceptEnderInventory(EnderInventoryAcceptEvent e)
	{
		EntityPlayer player = Minecraft.getMinecraft().player;
		if(player != null)
			enderChestTop8Items = TopStackHelper.getTopItems(player.getInventoryEnderChest(), 8);
	}
}