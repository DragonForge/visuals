package com.zeitheron.visuals.proxy;

import java.lang.reflect.Field;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.PacketSyncEnderChest;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.visuals.Visuals;
import com.zeitheron.visuals.api.ItemChangesTracker;
import com.zeitheron.visuals.net.PacketPutRenderer;
import com.zeitheron.visuals.net.PacketRemoveRenderer;
import com.zeitheron.visuals.util.TopStackHelper;

import net.minecraft.block.BlockAnvil;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.ContainerEnchantment;
import net.minecraft.inventory.ContainerRepair;
import net.minecraft.inventory.ContainerWorkbench;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityEnchantmentTable;
import net.minecraft.tileentity.TileEntityEnderChest;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;

public class CommonProxy
{
	@SubscribeEvent
	public final void openContainer(PlayerContainerEvent.Open e)
	{
		EntityPlayerMP mp = WorldUtil.cast(e.getEntityPlayer(), EntityPlayerMP.class);
		if(mp == null)
			return;
		World world = mp.world;
		Container c = e.getContainer();
		
		if(c instanceof ContainerEnchantment)
		{
			BlockPos tablePos = BlockPos.ORIGIN;
			
			Field f = ContainerEnchantment.class.getDeclaredFields()[2];
			f.setAccessible(true);
			try
			{
				tablePos = (BlockPos) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			final BlockPos pos = tablePos;
			ContainerEnchantment ce = (ContainerEnchantment) c;
			
			boolean isTable = world.isBlockLoaded(tablePos) && world.getTileEntity(tablePos) instanceof TileEntityEnchantmentTable;
			
			if(isTable)
				c.addListener(new ItemChangesTracker(-1, slot ->
				{
					if(ce.tableInventory == slot.inventory)
					{
						NonNullList<ItemStack> items = NonNullList.withSize(2, ItemStack.EMPTY);
						items.set(0, ce.tableInventory.getStackInSlot(0));
						items.set(1, ce.tableInventory.getStackInSlot(1));
						if(mp.capabilities.isCreativeMode && !items.get(0).isEmpty())
							items.set(1, new ItemStack(Items.DYE, 3, 4));
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withItems(items).withPos(pos), new TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 256));
					}
				}));
		}
		
		if(c instanceof ContainerRepair)
		{
			BlockPos anvilPos = BlockPos.ORIGIN;
			
			IInventory inputSlots = null;
			IInventory outputSlots = null;
			
			try
			{
				Field f = ContainerRepair.class.getDeclaredFields()[1];
				f.setAccessible(true);
				outputSlots = (IInventory) f.get(c);
				
				f = ContainerRepair.class.getDeclaredFields()[2];
				f.setAccessible(true);
				inputSlots = (IInventory) f.get(c);
				
				f = ContainerRepair.class.getDeclaredFields()[4];
				f.setAccessible(true);
				anvilPos = (BlockPos) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			final BlockPos pos = anvilPos;
			final IInventory inputInv = inputSlots, outputInv = outputSlots;
			
			boolean isAnvil = world.isBlockLoaded(anvilPos) && world.getBlockState(anvilPos).getBlock() instanceof BlockAnvil;
			
			if(isAnvil)
				c.addListener(new ItemChangesTracker(-1, slot ->
				{
					if(inputInv == slot.inventory || outputInv == slot.inventory)
					{
						NonNullList<ItemStack> items = NonNullList.withSize(3, ItemStack.EMPTY);
						items.set(0, inputInv.getStackInSlot(0));
						items.set(1, inputInv.getStackInSlot(1));
						items.set(2, c.getInventory().get(2));
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withItems(items).withPos(pos), new TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 256));
					}
				}));
		}
		
		if(c instanceof ContainerChest)
		{
			ContainerChest cc = (ContainerChest) c;
			
			IInventory lowerChestInventory = null;
			
			try
			{
				Field f = ContainerChest.class.getDeclaredFields()[0];
				f.setAccessible(true);
				lowerChestInventory = (IInventory) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			final IInventory extern = lowerChestInventory;
			
			// First sync
			{
				if(extern instanceof TileEntityChest)
				{
					TileEntityChest chest = (TileEntityChest) extern;
					
					HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest, 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
				} else if(extern instanceof InventoryLargeChest)
				{
					TileEntityChest lower = null, upper = null;
					
					try
					{
						Field f = InventoryLargeChest.class.getDeclaredFields()[1];
						f.setAccessible(true);
						upper = (TileEntityChest) f.get(extern);
						
						f = InventoryLargeChest.class.getDeclaredFields()[2];
						f.setAccessible(true);
						lower = (TileEntityChest) f.get(extern);
					} catch(Exception e1)
					{
						e1.printStackTrace();
					}
					
					NonNullList<ItemStack> topItems = TopStackHelper.getTopItems(extern, 16);
					
					NonNullList<ItemStack> up = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
					NonNullList<ItemStack> down = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
					
					for(int i = 0; i < up.size(); ++i)
					{
						up.set(i, topItems.get(i));
						down.set(i, topItems.get(i + up.size()));
					}
					
					HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(upper.getPos()).withItems(up), new TargetPoint(upper.getWorld().provider.getDimension(), upper.getPos().getX() + .5, upper.getPos().getY() + .5, upper.getPos().getZ() + .5, 256));
					HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(lower.getPos()).withItems(down), new TargetPoint(lower.getWorld().provider.getDimension(), lower.getPos().getX() + .5, lower.getPos().getY() + .5, lower.getPos().getZ() + .5, 256));
				} else if(extern instanceof InventoryEnderChest)
				{
					Field f = InventoryEnderChest.class.getDeclaredFields()[0];
					f.setAccessible(true);
					try
					{
						TileEntityEnderChest ec = (TileEntityEnderChest) f.get(extern);
						if(ec != null) // Fix backpacks from other mods.
							for(EntityPlayerMP omp : ec.getWorld().getEntitiesWithinAABB(EntityPlayerMP.class, new AxisAlignedBB(ec.getPos()).grow(256)))
								HCNet.INSTANCE.sendTo(new PacketSyncEnderChest().withInventory(omp.getInventoryEnderChest()), omp);
					} catch(IllegalArgumentException | IllegalAccessException e1)
					{
						e1.printStackTrace();
					}
				}
			}
			
			c.addListener(new ItemChangesTracker(-1, slot ->
			{
				if(slot.inventory == extern)
				{
					if(extern instanceof TileEntityChest)
					{
						TileEntityChest chest = (TileEntityChest) extern;
						
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest, 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
					} else if(extern instanceof InventoryLargeChest)
					{
						TileEntityChest lower = null, upper = null;
						
						try
						{
							Field f = InventoryLargeChest.class.getDeclaredFields()[1];
							f.setAccessible(true);
							upper = (TileEntityChest) f.get(extern);
							
							f = InventoryLargeChest.class.getDeclaredFields()[2];
							f.setAccessible(true);
							lower = (TileEntityChest) f.get(extern);
						} catch(Exception e1)
						{
							e1.printStackTrace();
						}
						
						NonNullList<ItemStack> topItems = TopStackHelper.getTopItems(extern, 16);
						
						NonNullList<ItemStack> up = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
						NonNullList<ItemStack> down = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
						
						for(int i = 0; i < up.size(); ++i)
						{
							up.set(i, topItems.get(i));
							down.set(i, topItems.get(i + up.size()));
						}
						
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(upper.getPos()).withItems(up), new TargetPoint(upper.getWorld().provider.getDimension(), upper.getPos().getX() + .5, upper.getPos().getY() + .5, upper.getPos().getZ() + .5, 256));
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(lower.getPos()).withItems(down), new TargetPoint(lower.getWorld().provider.getDimension(), lower.getPos().getX() + .5, lower.getPos().getY() + .5, lower.getPos().getZ() + .5, 256));
					} else if(extern instanceof InventoryEnderChest)
						HCNet.INSTANCE.sendTo(new PacketSyncEnderChest().withInventory(mp.getInventoryEnderChest()), mp);
				}
			}));
		}
		
		if(c instanceof ContainerWorkbench)
		{
			BlockPos pos = null;
			
			try
			{
				Field f = ContainerWorkbench.class.getDeclaredFields()[3];
				f.setAccessible(true);
				pos = (BlockPos) f.get(c);
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
			
			ContainerWorkbench cw = (ContainerWorkbench) c;
			final BlockPos fpos = pos;
			
			c.addListener(new ItemChangesTracker(-1, slot ->
			{
				if(slot.inventory == cw.craftMatrix || slot.inventory == cw.craftResult)
				{
					NonNullList<ItemStack> list = NonNullList.withSize(10, ItemStack.EMPTY);
					for(int i = 0; i < 9; ++i)
						list.set(i, cw.craftMatrix.getStackInSlot(i));
					list.set(9, cw.craftResult.getStackInSlot(0));
					HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(fpos).withItems(list), new TargetPoint(mp.world.provider.getDimension(), fpos.getX() + .5, fpos.getY() + .5, fpos.getZ() + .5, 256));
				}
			}));
		}
		
		Visuals.compats.forEach(v -> v.containerOpen(e));
	}
	
	@SubscribeEvent
	public final void closeContainer(PlayerContainerEvent.Close e)
	{
		EntityPlayerMP mp = WorldUtil.cast(e.getEntityPlayer(), EntityPlayerMP.class);
		if(mp == null)
			return;
		World world = mp.world;
		Container c = e.getContainer();
		
		if(c instanceof ContainerEnchantment)
		{
			BlockPos tablePos = BlockPos.ORIGIN;
			
			Field f = ContainerEnchantment.class.getDeclaredFields()[2];
			f.setAccessible(true);
			try
			{
				tablePos = (BlockPos) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			boolean isTable = world.isBlockLoaded(tablePos) && world.getTileEntity(tablePos) instanceof TileEntityEnchantmentTable;
			
			if(isTable)
				HCNet.INSTANCE.sendToAllAround(new PacketRemoveRenderer().withPos(tablePos), new TargetPoint(world.provider.getDimension(), tablePos.getX(), tablePos.getY(), tablePos.getZ(), 256));
		}
		
		if(c instanceof ContainerRepair)
		{
			BlockPos anvilPos = BlockPos.ORIGIN;
			
			Field f = ContainerRepair.class.getDeclaredFields()[4];
			f.setAccessible(true);
			try
			{
				anvilPos = (BlockPos) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			boolean isAnvil = world.isBlockLoaded(anvilPos) && world.getBlockState(anvilPos).getBlock() instanceof BlockAnvil;
			
			if(isAnvil)
				HCNet.INSTANCE.sendToAllAround(new PacketRemoveRenderer().withPos(anvilPos), new TargetPoint(world.provider.getDimension(), anvilPos.getX(), anvilPos.getY(), anvilPos.getZ(), 256));
		}
		
		if(c instanceof ContainerWorkbench)
		{
			BlockPos pos = null;
			
			try
			{
				Field f = ContainerWorkbench.class.getDeclaredFields()[3];
				f.setAccessible(true);
				pos = (BlockPos) f.get(c);
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
			
			HCNet.INSTANCE.sendToAllAround(new PacketRemoveRenderer().withPos(pos), new TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 256));
		}
		
		Visuals.compats.forEach(v -> v.containerClose(e));
	}
	
	public void init()
	{
	}
	
	public ResourceLocation trimToHead(ResourceLocation skin)
	{
		return skin;
	}
}