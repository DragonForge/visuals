package com.zeitheron.visuals.world;

import com.zeitheron.visuals.api.FallingFix;

import net.minecraft.block.BlockFalling;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.world.World;

public class WFFallableBlockFix extends WFBase
{
	{
		setRegistryName("visuals", "fallable");
	}
	
	@Override
	public void processPillar(World world, MutableBlockPos pos, int x, int z)
	{
		IBlockState prevState = null, state = null;
		
		for(int y = 0; y < world.getHeight(); ++y)
		{
			pos.setY(y);
			
			prevState = state;
			state = world.getBlockState(pos);
			
			if(prevState != null && state != null && prevState.getBlock() == Blocks.AIR && state.getBlock() instanceof BlockFalling)
				world.setBlockState(pos, FallingFix.getUnfallable(state));
		}
	}
}