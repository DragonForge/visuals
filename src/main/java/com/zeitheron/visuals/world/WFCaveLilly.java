package com.zeitheron.visuals.world;

import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.world.World;

public class WFCaveLilly extends WFBase
{
	{
		setRegistryName("visuals", "waterlilly");
	}
	
	@Override
	public void processPillar(World world, MutableBlockPos pos, int x, int z)
	{
		int h = world.getSeaLevel() - 5;
		for(int y = 0; y < h; ++y)
		{
			pos.setY(y);
			
			if(world.getBlockState(pos).getBlock() == Blocks.WATERLILY)
				world.setBlockToAir(pos);
		}
	}
}