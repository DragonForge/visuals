package com.zeitheron.visuals.world;

import java.util.Random;

import com.zeitheron.hammercore.utils.ChunkUtils;
import com.zeitheron.hammercore.world.gen.IWorldGenFeature;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.registries.IForgeRegistryEntry;

public abstract class WFBase extends IForgeRegistryEntry.Impl<WFBase> implements IWorldGenFeature<WFBase>
{
	public abstract void processPillar(World world, MutableBlockPos pos, int x, int z);
	
	@Override
	public int getMaxChances(World world, ChunkPos chunk, Random rand)
	{
		MutableBlockPos pos = new MutableBlockPos();
		for(int x = 0; x < 16; ++x)
			for(int z = 0; z < 16; ++z)
			{
				pos.setPos(ChunkUtils.getChunkPos(chunk.x, chunk.z, x, 0, z));
				processPillar(world, pos, x, z);
			}
		
		return 1;
	}
	
	@Override
	public int getMinY(World world, BlockPos pos, Random rand)
	{
		return 0;
	}
	
	@Override
	public int getMaxY(World world, BlockPos pos, Random rand)
	{
		return 255;
	}
	
	@Override
	public void generate(World world, BlockPos pos, Random rand)
	{
	}
}