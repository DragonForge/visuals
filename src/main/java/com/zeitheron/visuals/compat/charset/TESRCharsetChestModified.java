package com.zeitheron.visuals.compat.charset;

import static com.zeitheron.visuals.client.tesr.TESRChestModified.CHEST_INSIDE;
import static com.zeitheron.visuals.client.tesr.TESRChestModified.CHEST_LARGE_INSIDE;
import static com.zeitheron.visuals.client.tesr.TESRChestModified.customitem;
import static com.zeitheron.visuals.client.tesr.TESRChestModified.getRenderEntityItem;
import static com.zeitheron.visuals.client.tesr.TESRChestModified.shifts;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Random;

import com.zeitheron.visuals.proxy.ClientProxy;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import pl.asie.charset.lib.block.TraitMaterial;
import pl.asie.charset.lib.utils.RenderUtils;
import pl.asie.charset.lib.utils.RenderUtils.AveragingMode;
import pl.asie.charset.module.storage.chests.TileEntityChestCharset;
import pl.asie.charset.module.storage.chests.TileEntityChestRendererCharset;

public class TESRCharsetChestModified extends TileEntityChestRendererCharset
{
	static Method getLidAnglem;
	static Field materialf;
	
	public static float getLidAngle(TileEntityChestCharset chest, float partialTicks)
	{
		try
		{
			if(getLidAnglem == null)
			{
				getLidAnglem = chest.getClass().getDeclaredMethod("getLidAngle", float.class);
				getLidAnglem.setAccessible(true);
			}
			if(getLidAnglem != null)
				return Float.class.cast(getLidAnglem.invoke(chest, partialTicks));
		} catch(ReflectiveOperationException | SecurityException e)
		{
			e.printStackTrace();
		}
		return 0;
	}
	
	public static TraitMaterial getMaterial(TileEntityChestCharset chest)
	{
		try
		{
			if(materialf == null)
			{
				materialf = chest.getClass().getDeclaredField("material");
				materialf.setAccessible(true);
			}
			if(materialf != null)
				return TraitMaterial.class.cast(materialf.get(chest));
		} catch(ReflectiveOperationException | SecurityException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public void render(TileEntityChestCharset te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		
		boolean doubl = te.hasNeighbor();
		
		float f = getLidAngle(te, partialTicks);
		
		if(f > 0F)
		{
			TraitMaterial mat = getMaterial(te);
			if(mat != null)
				RenderUtils.glColor(RenderUtils.getAverageColor(RenderUtils.getItemSprite(mat.getMaterial().getStack(), te.getWorld(), null, null), AveragingMode.FULL), alpha);
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(x, y + .999F, z + 1);
			GlStateManager.rotate(180, 1, 0, 0);
			if(!doubl)
				CHEST_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
			else
			{
				if(te.getNeighborFace() == EnumFacing.EAST)
					CHEST_LARGE_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
				if(te.getNeighborFace() == EnumFacing.SOUTH)
				{
					GlStateManager.translate(0, 0, 1);
					GlStateManager.rotate(90, 0, 1, 0);
					CHEST_LARGE_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
				}
			}
			GlStateManager.popMatrix();
			
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			
			NonNullList<ItemStack> list = ClientProxy.renderData.get(te.getPos());
			
			if(list != null)
			{
				GlStateManager.pushMatrix();
				GlStateManager.translate(x + .5F, y - .6F, z + .5F);
				GlStateManager.enableCull();
				Random random = new Random();
				
				random.setSeed(254L);
				int shift = 0;
				float blockScale = .6F;
				float distMult = 1F;
				float timeD = (float) (360.0D * (double) (System.currentTimeMillis() & 16383L) / 16383.0D) - partialTicks;
				if(list.get(1).isEmpty())
				{
					shift = 8;
					blockScale = 0.85F;
				}
				
				GlStateManager.translate(-.5F, .5F, -.5F);
				customitem.setWorld(te.getWorld());
				customitem.hoverStart = 0;
				Iterator<ItemStack> var19 = list.iterator();
				
				while(var19.hasNext())
				{
					ItemStack item = var19.next();
					if(shift > shifts.length || shift > 8)
						break;
					
					if(item.isEmpty())
						++shift;
					else
					{
						float shiftX = shifts[shift][0] * distMult;
						float shiftY = shifts[shift][1] * distMult;
						float shiftZ = shifts[shift][2] * distMult;
						++shift;
						GlStateManager.pushMatrix();
						GlStateManager.translate(shiftX, shiftY, shiftZ);
						GlStateManager.rotate(timeD, 0.0F, 1.0F, 0.0F);
						GlStateManager.scale(blockScale, blockScale, blockScale);
						customitem.setItem(item);
						
						getRenderEntityItem().doRender(customitem, 0.0D, 0.0D, 0.0D, 0.0F, 0);
						GlStateManager.popMatrix();
					}
				}
				
				GlStateManager.popMatrix();
			}
		}
	}
}