package com.zeitheron.visuals.compat.charset;

import static com.zeitheron.visuals.client.tex.TextureTransformer.*;

import net.minecraftforge.fml.client.registry.ClientRegistry;
import pl.asie.charset.module.storage.chests.TileEntityChestCharset;

public class VCC extends VCS
{
	@Override
	public void init()
	{
		try
		{
			ClientRegistry.bindTileEntitySpecialRenderer(TileEntityChestCharset.class, new TESRCharsetChestModified());
			
			transform(tex -> tex.getNamespace().equals("charset_generated") && tex.getPath().startsWith("textures/entity/chest/") && tex.getPath().endsWith(".png") && !tex.getPath().endsWith("_double.png"), CHEST_SINGLE_SAW);
			transform(tex -> tex.getNamespace().equals("charset_generated") && tex.getPath().startsWith("textures/entity/chest/") && tex.getPath().endsWith("_double.png"), CHEST_DOUBLE_SAW);
		} catch(Throwable err)
		{
			// Chest not found
		}
	}
}