package com.zeitheron.visuals.compat.charset;

import java.lang.reflect.Field;

import com.zeitheron.hammercore.mod.ModuleLoader;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.PacketSyncEnderChest;
import com.zeitheron.visuals.api.ItemChangesTracker;
import com.zeitheron.visuals.compat.base.VisualsCompat;
import com.zeitheron.visuals.net.PacketPutRenderer;
import com.zeitheron.visuals.util.TopStackHelper;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityEnderChest;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.event.entity.player.PlayerContainerEvent.Close;
import net.minecraftforge.event.entity.player.PlayerContainerEvent.Open;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;
import pl.asie.charset.module.storage.chests.ContainerChestCharset;
import pl.asie.charset.module.storage.chests.TileEntityChestCharset;

@ModuleLoader(requiredModid = "charset")
public class VisualCharset extends VisualsCompat
{
	@Override
	public void init()
	{
		((VCS) getProxy()).init();
	}
	
	@Override
	public void containerClose(Close e)
	{
	}
	
	@Override
	public void containerOpen(Open e)
	{
		Container c = e.getContainer();
		
		if(c instanceof ContainerChestCharset)
		{
			ContainerChestCharset cc = (ContainerChestCharset) c;
			
			TileEntityChestCharset lowerChestInventory = null;
			
			try
			{
				Field f = ContainerChestCharset.class.getDeclaredFields()[0];
				f.setAccessible(true);
				lowerChestInventory = (TileEntityChestCharset) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			final TileEntityChestCharset extern = lowerChestInventory;
			
			// First sync
			{
				if(extern != null)
				{
					TileEntityChestCharset chest = (TileEntityChestCharset) extern;
					
					if(chest.hasNeighbor())
					{
						NonNullList<ItemStack> topItems = TopStackHelper.getTopItems(chest, 16);
						
						NonNullList<ItemStack> up = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
						NonNullList<ItemStack> down = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
						
						for(int i = 0; i < up.size(); ++i)
						{
							up.set(i, topItems.get(i));
							down.set(i, topItems.get(i + up.size()));
						}
						
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(up), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getNeighbor().getPos()).withItems(down), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
					} else
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest, 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
				}
			}
			
			c.addListener(new ItemChangesTracker(-1, slot ->
			{
				if(slot.inventory instanceof InventoryBasic)
				{
					TileEntityChestCharset chest = (TileEntityChestCharset) extern;
					
					if(chest.hasNeighbor())
					{
						NonNullList<ItemStack> topItems = TopStackHelper.getTopItems(chest, 16);
						
						NonNullList<ItemStack> up = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
						NonNullList<ItemStack> down = NonNullList.withSize(topItems.size() / 2, ItemStack.EMPTY);
						
						for(int i = 0; i < up.size(); ++i)
						{
							up.set(i, topItems.get(i));
							down.set(i, topItems.get(i + up.size()));
						}
						
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(up), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getNeighbor().getPos()).withItems(down), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
					} else
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest, 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
				}
			}));
		}
	}
	
	@Override
	public String getClientProxyClass()
	{
		return "com.zeitheron.visuals.compat.charset.VCC";
	}
	
	@Override
	public String getServerProxyClass()
	{
		return "com.zeitheron.visuals.compat.charset.VCS";
	}
}