package com.zeitheron.visuals.compat.stonechest;

import static com.zeitheron.visuals.client.tex.TextureTransformer.*;

import ftblag.stonechest.tileentities.TileEntityStoneChest;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;

public class VSCC extends VSCS
{
	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityStoneChest.class, new TESRStoneChestModified());
		
		transform(new ResourceLocation("stonechest", "textures/entity/andesite.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/andesite_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/cobblestone.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/cobblestone_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/diorite.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/diorite_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/granite.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/granite_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/stone.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("stonechest", "textures/entity/stone_double.png"), CHEST_DOUBLE_SAW);
	}
}