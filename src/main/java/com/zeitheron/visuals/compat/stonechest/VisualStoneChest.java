package com.zeitheron.visuals.compat.stonechest;

import com.zeitheron.hammercore.mod.ModuleLoader;
import com.zeitheron.visuals.compat.base.VisualsCompat;

@ModuleLoader(requiredModid = "stonechest")
public class VisualStoneChest extends VisualsCompat
{
	@Override
	public void init()
	{
		((VSCS) getProxy()).init();
	}
	
	@Override
	public String getClientProxyClass()
	{
		return "com.zeitheron.visuals.compat.stonechest.VSCC";
	}
	
	@Override
	public String getServerProxyClass()
	{
		return "com.zeitheron.visuals.compat.stonechest.VSCS";
	}
}