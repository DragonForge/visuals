package com.zeitheron.visuals.compat.atum;

import com.zeitheron.hammercore.mod.ModuleLoader;
import com.zeitheron.visuals.compat.base.VisualsCompat;

@ModuleLoader(requiredModid = "atum")
public class VisualAtum extends VisualsCompat
{
	@Override
	public void init()
	{
		((VAS) getProxy()).init();
	}
	
	@Override
	public String getClientProxyClass()
	{
		return "com.zeitheron.visuals.compat.atum.VAC";
	}
	
	@Override
	public String getServerProxyClass()
	{
		return "com.zeitheron.visuals.compat.atum.VAS";
	}
}