package com.zeitheron.visuals.compat.atum;

import static com.zeitheron.visuals.client.tex.TextureTransformer.*;

import com.teammetallurgy.atum.blocks.stone.limestone.chest.tileentity.TileEntityLimestoneChest;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;

public class VAC extends VAS
{
	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityLimestoneChest.class, new TESRChestBaseModified());
		
		transform(new ResourceLocation("atum", "textures/blocks/chest/limestone_chest.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("atum", "textures/blocks/chest/limestone_chest_double.png"), CHEST_DOUBLE_SAW_CI);
	}
}