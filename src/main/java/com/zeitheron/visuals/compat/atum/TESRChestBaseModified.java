package com.zeitheron.visuals.compat.atum;

import static com.zeitheron.visuals.client.tesr.TESRChestModified.*;

import java.util.Iterator;
import java.util.Random;

import com.teammetallurgy.atum.blocks.base.tileentity.TileEntityChestBase;
import com.teammetallurgy.atum.client.render.tileentity.RenderTileChest;
import com.zeitheron.visuals.proxy.ClientProxy;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class TESRChestBaseModified extends RenderTileChest
{
	@Override
	public void render(TileEntityChestBase te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		
		boolean doubl = te.adjacentChestXNeg != null || te.adjacentChestXPos != null || te.adjacentChestZNeg != null || te.adjacentChestZPos != null;
		
		float f = te.prevLidAngle + (te.lidAngle - te.prevLidAngle) * partialTicks;
		
		if(te.adjacentChestZNeg != null)
		{
			float f1 = te.adjacentChestZNeg.prevLidAngle + (te.adjacentChestZNeg.lidAngle - te.adjacentChestZNeg.prevLidAngle) * partialTicks;
			
			if(f1 > f)
			{
				f = f1;
			}
		}
		
		if(te.adjacentChestXNeg != null)
		{
			float f2 = te.adjacentChestXNeg.prevLidAngle + (te.adjacentChestXNeg.lidAngle - te.adjacentChestXNeg.prevLidAngle) * partialTicks;
			
			if(f2 > f)
			{
				f = f2;
			}
		}
		
		if(f > 0F)
		{
			GlStateManager.pushMatrix();
			GlStateManager.translate(x, y + .999F, z + 1);
			GlStateManager.rotate(180, 1, 0, 0);
			if(!doubl)
				CHEST_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
			else
			{
				if(te.adjacentChestXPos != null)
					CHEST_LARGE_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
				if(te.adjacentChestZPos != null)
				{
					GlStateManager.translate(0, 0, 1);
					GlStateManager.rotate(90, 0, 1, 0);
					CHEST_LARGE_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
				}
			}
			GlStateManager.popMatrix();
			
			NonNullList<ItemStack> list = ClientProxy.renderData.get(te.getPos());
			
			if(list != null)
			{
				GlStateManager.pushMatrix();
				GlStateManager.translate(x + .5F, y - .6F, z + .5F);
				GlStateManager.enableCull();
				Random random = new Random();
				
				random.setSeed(254L);
				int shift = 0;
				float blockScale = .6F;
				float distMult = 1F;
				float timeD = (float) (360.0D * (double) (System.currentTimeMillis() & 16383L) / 16383.0D) - partialTicks;
				if(list.get(1).isEmpty())
				{
					shift = 8;
					blockScale = 0.85F;
				}
				
				GlStateManager.translate(-.5F, .5F, -.5F);
				customitem.setWorld(te.getWorld());
				customitem.hoverStart = 0;
				Iterator<ItemStack> var19 = list.iterator();
				
				while(var19.hasNext())
				{
					ItemStack item = var19.next();
					if(shift > shifts.length || shift > 8)
						break;
					
					if(item.isEmpty())
						++shift;
					else
					{
						float shiftX = shifts[shift][0] * distMult;
						float shiftY = shifts[shift][1] * distMult;
						float shiftZ = shifts[shift][2] * distMult;
						++shift;
						GlStateManager.pushMatrix();
						GlStateManager.translate(shiftX, shiftY, shiftZ);
						GlStateManager.rotate(timeD, 0.0F, 1.0F, 0.0F);
						GlStateManager.scale(blockScale, blockScale, blockScale);
						customitem.setItem(item);
						
						getRenderEntityItem().doRender(customitem, 0.0D, 0.0D, 0.0D, 0.0F, 0);
						GlStateManager.popMatrix();
					}
				}
				
				GlStateManager.popMatrix();
			}
		}
	}
}