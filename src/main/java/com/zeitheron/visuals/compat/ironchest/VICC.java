package com.zeitheron.visuals.compat.ironchest;

import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_SINGLE_SAW;
import static com.zeitheron.visuals.client.tex.TextureTransformer.transform;

import cpw.mods.ironchest.common.tileentity.chest.TileEntityCopperChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityDiamondChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityGoldChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityIronChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityObsidianChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntitySilverChest;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;

public class VICC extends VICS
{
	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCopperChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDiamondChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityGoldChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityIronChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityObsidianChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySilverChest.class, new TESRIronChestModified());
		
		transform(new ResourceLocation("ironchest", "textures/model/chest/copper_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/diamond_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/dirt_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/gold_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/iron_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/obsidian_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/silver_chest.png"), CHEST_SINGLE_SAW);
	}
}