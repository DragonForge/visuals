package com.zeitheron.visuals.compat.ironchest;

import java.lang.reflect.Field;

import com.zeitheron.hammercore.mod.ModuleLoader;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.visuals.api.ItemChangesTracker;
import com.zeitheron.visuals.compat.base.VisualsCompat;
import com.zeitheron.visuals.net.PacketPutRenderer;
import com.zeitheron.visuals.util.TopStackHelper;

import cpw.mods.ironchest.common.gui.chest.ContainerIronChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityIronChest;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerContainerEvent.Open;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;

@ModuleLoader(requiredModid = "ironchest")
public class VisualIronChest extends VisualsCompat
{
	@Override
	public void init()
	{
		((VICS) getProxy()).init();
	}

	@Override
	public void containerOpen(Open e)
	{
		EntityPlayerMP mp = WorldUtil.cast(e.getEntityPlayer(), EntityPlayerMP.class);
		if(mp == null)
			return;
		World world = mp.world;
		Container c = e.getContainer();
		
		if(c instanceof ContainerIronChest)
		{
			ContainerIronChest cc = (ContainerIronChest) c;
			
			IInventory inventory = null;
			
			try
			{
				Field f = ContainerIronChest.class.getDeclaredFields()[2];
				f.setAccessible(true);
				inventory = (IInventory) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			final IInventory extern = inventory;
			
			// First sync
			{
				if(extern instanceof TileEntityIronChest)
				{
					TileEntityIronChest chest = (TileEntityIronChest) extern;
					
					HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest, 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
				}
			}
			
			c.addListener(new ItemChangesTracker(-1, slot ->
			{
				if(slot.inventory == extern)
				{
					if(extern instanceof TileEntityIronChest)
					{
						TileEntityIronChest chest = (TileEntityIronChest) extern;
						
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest, 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
					}
				}
			}));
		}
	}
	
	@Override
	public String getClientProxyClass()
	{
		return "com.zeitheron.visuals.compat.ironchest.VICC";
	}

	@Override
	public String getServerProxyClass()
	{
		return "com.zeitheron.visuals.compat.ironchest.VICS";
	}
}