package com.zeitheron.visuals.compat.metalchests;

import java.lang.reflect.Field;

import com.zeitheron.hammercore.mod.ModuleLoader;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.visuals.api.ItemChangesTracker;
import com.zeitheron.visuals.compat.base.VisualsCompat;
import com.zeitheron.visuals.net.PacketPutRenderer;
import com.zeitheron.visuals.util.TopStackHelper;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerContainerEvent.Close;
import net.minecraftforge.event.entity.player.PlayerContainerEvent.Open;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;
import t145.metalchests.api.chests.IMetalChest;
import t145.metalchests.containers.ContainerMetalChest;
import t145.metalchests.tiles.TileMetalChest;

@ModuleLoader(requiredModid = "metalchests")
public class VisualMetalChests extends VisualsCompat
{
	@Override
	public void preInit()
	{
		((VMCS) getProxy()).preInit();
	}
	
	@Override
	public void init()
	{
		((VMCS) getProxy()).init();
	}
	
	@Override
	public void containerOpen(Open e)
	{
		EntityPlayerMP mp = WorldUtil.cast(e.getEntityPlayer(), EntityPlayerMP.class);
		if(mp == null)
			return;
		World world = mp.world;
		Container c = e.getContainer();
		
		if(c instanceof ContainerMetalChest)
		{
			ContainerMetalChest cc = (ContainerMetalChest) c;
			
			IMetalChest inventory = null;
			
			try
			{
				Field f = ContainerMetalChest.class.getDeclaredFields()[2];
				f.setAccessible(true);
				inventory = (IMetalChest) f.get(c);
			} catch(IllegalArgumentException | IllegalAccessException e1)
			{
				e1.printStackTrace();
			}
			
			final IMetalChest extern = inventory;
			
			// First sync
			{
				if(extern instanceof TileMetalChest)
				{
					TileMetalChest chest = (TileMetalChest) extern;
					HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest.getInventory(), 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
				}
			}
			
			c.addListener(new ItemChangesTracker(-1, slot ->
			{
				if(slot.inventory instanceof InventoryBasic)
				{
					if(extern instanceof TileMetalChest)
					{
						TileMetalChest chest = (TileMetalChest) extern;
						
						HCNet.INSTANCE.sendToAllAround(new PacketPutRenderer().withPos(chest.getPos()).withItems(TopStackHelper.getTopItems(chest.getInventory(), 8)), new TargetPoint(chest.getWorld().provider.getDimension(), chest.getPos().getX() + .5, chest.getPos().getY() + .5, chest.getPos().getZ() + .5, 256));
					}
				}
			}));
		}
	}
	
	@Override
	public void containerClose(Close e)
	{
	}
	
	@Override
	public String getClientProxyClass()
	{
		return "com.zeitheron.visuals.compat.metalchests.VMCC";
	}
	
	@Override
	public String getServerProxyClass()
	{
		return "com.zeitheron.visuals.compat.metalchests.VMCS";
	}
}