package com.zeitheron.visuals.compat.metalchests;

import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_SINGLE_SAW_CI;
import static com.zeitheron.visuals.client.tex.TextureTransformer.transform;

import com.zeitheron.hammercore.utils.FinalFieldHelper;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import t145.metalchests.client.render.blocks.RenderMetalChest;
import t145.metalchests.tiles.TileMetalChest;

public class VMCC extends VMCS
{
	@Override
	public void preInit()
	{
		FinalFieldHelper.setStaticFinalField(RenderMetalChest.class, "INSTANCE", new TESRMetalChestModified());
	}
	
	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileMetalChest.class, RenderMetalChest.INSTANCE);
		
		transform(new ResourceLocation("metalchests", "textures/entity/chest/copper.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/copper_h.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/diamond.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/diamond_h.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/gold.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/gold_h.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/iron.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/iron_h.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/obsidian.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/obsidian_h.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/silver.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/silver_h.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/hungry/copper.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/hungry/diamond.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/hungry/gold.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/hungry/iron.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/hungry/obsidian.png"), CHEST_SINGLE_SAW_CI);
		transform(new ResourceLocation("metalchests", "textures/entity/chest/hungry/silver.png"), CHEST_SINGLE_SAW_CI);
	}
}