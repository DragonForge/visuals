package com.zeitheron.visuals.compat.metalchests;

import static com.zeitheron.visuals.client.tesr.TESRChestModified.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Random;

import com.zeitheron.visuals.proxy.ClientProxy;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import t145.metalchests.client.render.blocks.RenderMetalChest;
import t145.metalchests.tiles.TileMetalChest;

public class TESRMetalChestModified extends RenderMetalChest
{
	static Field animatorf;
	static Method getLidAnglem;
	
	public static float getLidAngle(TileMetalChest tmc, float partialTicks)
	{
		if(animatorf == null)
			try
			{
				animatorf = tmc.getClass().getDeclaredField("animator");
				animatorf.setAccessible(true);
			} catch(NoSuchFieldException | SecurityException e)
			{
				e.printStackTrace();
			}
		if(animatorf != null)
		{
			try
			{
				Object ourAnimator = animatorf.get(tmc);
				
				if(getLidAnglem == null)
				{
					getLidAnglem = ourAnimator.getClass().getDeclaredMethod("getLidAngle", float.class);
					getLidAnglem.setAccessible(true);
				}
				
				return -Float.class.cast(getLidAnglem.invoke(ourAnimator, partialTicks));
			} catch(IllegalArgumentException | ReflectiveOperationException | SecurityException e)
			{
				e.printStackTrace();
			}
		}
		return 0F;
	}
	
	@Override
	public void render(TileMetalChest te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		
		float f = getLidAngle(te, partialTicks);
		
		if(f > 0F && destroyStage == -1)
		{
			GlStateManager.pushMatrix();
			GlStateManager.translate(x, y + .999F, z + 1);
			GlStateManager.rotate(180, 1, 0, 0);
			
			CHEST_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
			
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			
			GlStateManager.popMatrix();
			
			NonNullList<ItemStack> list = ClientProxy.renderData.get(te.getPos());
			
			if(list != null && !list.isEmpty())
			{
				GlStateManager.pushMatrix();
				GlStateManager.translate(x + .5F, y - .6F, z + .5F);
				GlStateManager.enableCull();
				Random random = new Random();
				
				random.setSeed(254L);
				int shift = 0;
				float blockScale = .6F;
				float distMult = 1F;
				float timeD = (float) (360.0D * (double) (System.currentTimeMillis() & 16383L) / 16383.0D) - partialTicks;
				if(list.get(1).isEmpty())
				{
					shift = 8;
					blockScale = 0.85F;
				}
				
				GlStateManager.translate(-.5F, .5F, -.5F);
				customitem.setWorld(te.getWorld());
				customitem.hoverStart = 0;
				Iterator<ItemStack> var19 = list.iterator();
				
				while(var19.hasNext())
				{
					ItemStack item = var19.next();
					if(shift > shifts.length || shift > 8)
						break;
					
					if(item.isEmpty())
						++shift;
					else
					{
						float shiftX = shifts[shift][0] * distMult;
						float shiftY = shifts[shift][1] * distMult;
						float shiftZ = shifts[shift][2] * distMult;
						++shift;
						GlStateManager.pushMatrix();
						GlStateManager.translate(shiftX, shiftY, shiftZ);
						GlStateManager.rotate(timeD, 0.0F, 1.0F, 0.0F);
						GlStateManager.scale(blockScale, blockScale, blockScale);
						customitem.setItem(item);
						
						getRenderEntityItem().doRender(customitem, 0.0D, 0.0D, 0.0D, 0.0F, 0);
						GlStateManager.popMatrix();
					}
				}
				
				GlStateManager.popMatrix();
			}
		}
	}
}