package com.zeitheron.visuals.compat.quark;

import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_DOUBLE_SAW;
import static com.zeitheron.visuals.client.tex.TextureTransformer.CHEST_SINGLE_SAW;
import static com.zeitheron.visuals.client.tex.TextureTransformer.transform;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import vazkii.quark.decoration.tile.TileCustomChest;

public class VQC extends VQS
{
	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileCustomChest.class, new TESRQuarkChestModified());
		
		transform(new ResourceLocation("quark", "textures/blocks/chests/acacia.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/acacia_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/birch.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/birch_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/dark_oak.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/dark_oak_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/jungle.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/jungle_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/spruce.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("quark", "textures/blocks/chests/spruce_double.png"), CHEST_DOUBLE_SAW);
	}
}