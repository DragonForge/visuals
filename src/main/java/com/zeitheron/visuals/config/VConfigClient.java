package com.zeitheron.visuals.config;

import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;

@HCModConfigurations(modid = "visuals", isModule = true, module = "client")
public class VConfigClient implements IConfigReloadListener
{
	@ModConfigPropertyBool(category = "animations", name = "Jukebox Animation", comment = "Should Visuals render a music disc inside the jukebox?\nTurn off if you are using a resource pack that looks weird with this feature on.", defaultValue = true)
	public static boolean animations_jukebox;
}