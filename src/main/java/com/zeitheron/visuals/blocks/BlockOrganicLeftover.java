package com.zeitheron.visuals.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockOrganicLeftover extends Block
{
	public static final AxisAlignedBB LEFTOVER_AABB = new AxisAlignedBB(1 / 16D, 0, 1 / 16D, 15 / 16D, 1 / 8D, 15 / 16D);
	
	public BlockOrganicLeftover()
	{
		super(Material.GROUND);
		setTranslationKey("organic_leftovers");
		setSoundType(SoundType.GROUND);
		setHardness(.25F);
		setHarvestLevel("shovel", 0);
	}
	
	@Override
	public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		Random rand = world instanceof World ? ((World)world).rand : RANDOM;
		
		if(rand.nextInt(6) == 0)
		{
			int q = 1 + rand.nextInt(5);
			for(int i = 0; i < q; ++i)
				drops.add(new ItemStack(Items.ROTTEN_FLESH));
		} else if(rand.nextInt(5) == 0)
		{
			int q = 1 + rand.nextInt(4);
			for(int i = 0; i < q; ++i)
				drops.add(new ItemStack(Blocks.DIRT));
		} else
		{
			int q = 1 + rand.nextInt(3);
			for(int i = 0; i < q; ++i)
				drops.add(new ItemStack(Items.STICK));
		}
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return LEFTOVER_AABB;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
}