package com.zeitheron.visuals.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.visuals.proxy.ClientProxy;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketRemoveRenderer implements IPacket
{
	static
	{
		IPacket.handle(PacketRemoveRenderer.class, PacketRemoveRenderer::new);
	}
	
	public BlockPos pos;
	
	public PacketRemoveRenderer withPos(BlockPos pos)
	{
		this.pos = pos;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("Pos", pos.toLong());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		pos = BlockPos.fromLong(nbt.getLong("Pos"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		ClientProxy.renderData.remove(pos);
		return null;
	}
}