package com.zeitheron.visuals.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.visuals.proxy.ClientProxy;

import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketPutRenderer implements IPacket
{
	static
	{
		IPacket.handle(PacketPutRenderer.class, PacketPutRenderer::new);
	}
	
	public BlockPos pos;
	public NonNullList<ItemStack> stacks;
	
	public PacketPutRenderer withPos(BlockPos pos)
	{
		this.pos = pos;
		return this;
	}
	
	public PacketPutRenderer withItems(NonNullList<ItemStack> stacks)
	{
		this.stacks = stacks;
		return this;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("Pos", pos.toLong());
		nbt.setInteger("SlotCount", stacks.size());
		ItemStackHelper.saveAllItems(nbt, stacks, true);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		pos = BlockPos.fromLong(nbt.getLong("Pos"));
		
		NBTTagList l = nbt.getTagList("Items", 10);
		stacks = NonNullList.withSize(nbt.getInteger("SlotCount"), ItemStack.EMPTY);
		ItemStackHelper.loadAllItems(nbt, stacks);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		ClientProxy.renderData.put(pos, stacks);
		return null;
	}
}