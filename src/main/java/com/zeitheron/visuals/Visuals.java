package com.zeitheron.visuals;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.mod.ModuleLister;
import com.zeitheron.hammercore.mod.ModuleLoader;
import com.zeitheron.hammercore.utils.FinalFieldHelper;
import com.zeitheron.hammercore.utils.ForgeRegistryUtils;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;
import com.zeitheron.visuals.blocks.BlockJukeboxFixed;
import com.zeitheron.visuals.blocks.BlockSkullFixed;
import com.zeitheron.visuals.compat.base.VisualsCompat;
import com.zeitheron.visuals.config.VConfigWorldGen;
import com.zeitheron.visuals.init.BlocksV;
import com.zeitheron.visuals.proxy.CommonProxy;
import com.zeitheron.visuals.tiles.TileEntityJukeboxFixed;
import com.zeitheron.visuals.world.WFCaveLilly;
import com.zeitheron.visuals.world.WFFallableBlockFix;
import com.zeitheron.visuals.world.WFOrganicLeftovers;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@EventBusSubscriber
@Mod(modid = "visuals", name = "Visuals", version = "@VERSION@", dependencies = "required-after:hammercore;after:ironchest;after:metalchests;after:quark;after:stonechest;after:atum;after:charset", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", updateJSON = "https://pastebin.com/raw/6jUK8A52")
public class Visuals
{
	public static final Logger LOG = LogManager.getLogger("Visuals");
	
	@SidedProxy(serverSide = "com.zeitheron.visuals.proxy.CommonProxy", clientSide = "com.zeitheron.visuals.proxy.ClientProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with Visuals jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://www.curseforge.com/projects/305723 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put("visuals", "https://www.curseforge.com/projects/305723");
	}
	
	public static List<VisualsCompat> compats = new ArrayList<>();
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		MinecraftForge.EVENT_BUS.register(proxy);
		
		compats = ModuleLister.createModules(VisualsCompat.class, null, e.getAsmData());
		
		for(VisualsCompat vc : compats)
		{
			ModuleLoader ml = vc.getClass().getAnnotation(ModuleLoader.class);
			if(ml != null)
				LOG.info("Attached compat for mod '" + Loader.instance().getIndexedModList().get(ml.requiredModid()).getName() + "': " + vc.getClass().getName());
		}
		
		compats.forEach(VisualsCompat::createProxy);
		
		compats.forEach(c -> c.preInit(e.getAsmData()));
		
		// Disable the block
		if(!VConfigWorldGen.feature_organicLeftovers)
			FinalFieldHelper.setStaticFinalField(BlocksV.class, "ORGANIC_LEFTOVERS", null);
		
		SimpleRegistration.registerFieldBlocksFrom(BlocksV.class, "visuals", CreativeTabs.DECORATIONS);
		ForgeRegistryUtils.overrideTileEntity("jukebox", TileEntityJukeboxFixed.class);
		
		if(VConfigWorldGen.fix_falling)
			WorldRetroGen.addWorldFeature(new WFFallableBlockFix());
		if(VConfigWorldGen.fix_lilypads)
			WorldRetroGen.addWorldFeature(new WFCaveLilly());
		if(VConfigWorldGen.feature_organicLeftovers)
			WorldRetroGen.addWorldFeature(new WFOrganicLeftovers());
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
		compats.forEach(VisualsCompat::init);
	}
	
	@SubscribeEvent
	public static void onBlockRegistry(RegistryEvent.Register<Block> e)
	{
		ModContainer minecraft = Loader.instance().getMinecraftModContainer();
		ModContainer active = Loader.instance().activeModContainer();
		
		List<Block> blocks = new ArrayList<>();
		
		LOG.info("************************");
		LOG.info("REGISTERING FIXES FOR VANILLA");
		LOG.info("************************");
		
		try
		{
			Loader.instance().setActiveModContainer(minecraft);
			
			{
				BlockSkullFixed fixed = new BlockSkullFixed();
				fixed.setRegistryName("minecraft", "skull");
				blocks.add(fixed);
				LOG.info("Fixed minecraft:skull.");
			}
			
			{
				BlockJukeboxFixed fixed = new BlockJukeboxFixed();
				fixed.setRegistryName("minecraft", "jukebox");
				blocks.add(fixed);
				LOG.info("Fixed minecraft:jukebox.");
			}
		} finally
		{
			Loader.instance().setActiveModContainer(active);
		}
		
		blocks.forEach(e.getRegistry()::register);
	}
}