package com.zeitheron.visuals.client.particle;

import javax.annotation.Nullable;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.particle.api.SimpleParticle;
import com.zeitheron.hammercore.client.utils.texture.TextureAtlasSpriteFull;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class FXDiggingTexture extends SimpleParticle
{
	public final ResourceLocation texture;
	public BlockPos sourcePos;
	
	public FXDiggingTexture(World worldIn, double xCoordIn, double yCoordIn, double zCoordIn, double xSpeedIn, double ySpeedIn, double zSpeedIn, ResourceLocation texture)
	{
		super(worldIn, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn);
		setParticleTexture(TextureAtlasSpriteFull.sprite);
		this.texture = texture;
		
		this.sourcePos = new BlockPos(this.posX, this.posY, this.posZ);
		
		this.particleGravity = Blocks.SKULL.blockParticleGravity;
		this.particleRed = 0.6F;
		this.particleGreen = 0.6F;
		this.particleBlue = 0.6F;
		this.particleScale /= 2.0F;
	}
	
	public FXDiggingTexture setBlockPos(BlockPos pos)
	{
		this.sourcePos = pos;
		multiplyColor(pos);
		return this;
	}
	
	protected void multiplyColor(@Nullable BlockPos pos)
	{
		int i = Minecraft.getMinecraft().getBlockColors().colorMultiplier(this.world.getBlockState(pos), this.world, pos, 0);
		this.particleRed *= (float) (i >> 16 & 255) / 255.0F;
		this.particleGreen *= (float) (i >> 8 & 255) / 255.0F;
		this.particleBlue *= (float) (i & 255) / 255.0F;
	}
	
	@Override
	public int getFXLayer()
	{
		return 1;
	}
	
	@Override
	public void doRenderParticle(double x, double y, double z, float partialTicks, float rotationX, float rotationZ, float rotationYZ, float rotationXY, float rotationXZ)
	{
		float f = ((float) this.particleTextureIndexX + this.particleTextureJitterX / 4.0F) / 16.0F;
		float f1 = f + 0.015609375F;
		float f2 = ((float) this.particleTextureIndexY + this.particleTextureJitterY / 4.0F) / 16.0F;
		float f3 = f2 + 0.015609375F;
		float f4 = 0.1F * this.particleScale;
		
		if(this.particleTexture != null)
		{
			f = this.particleTexture.getInterpolatedU((double) (this.particleTextureJitterX / 4.0F * 16.0F));
			f1 = this.particleTexture.getInterpolatedU((double) ((this.particleTextureJitterX + 1.0F) / 4.0F * 16.0F));
			f2 = this.particleTexture.getInterpolatedV((double) (this.particleTextureJitterY / 4.0F * 16.0F));
			f3 = this.particleTexture.getInterpolatedV((double) ((this.particleTextureJitterY + 1.0F) / 4.0F * 16.0F));
		}
		
		float f5 = (float) (this.prevPosX + (this.posX - this.prevPosX) * (double) partialTicks - interpPosX);
		float f6 = (float) (this.prevPosY + (this.posY - this.prevPosY) * (double) partialTicks - interpPosY);
		float f7 = (float) (this.prevPosZ + (this.posZ - this.prevPosZ) * (double) partialTicks - interpPosZ);
		int i = this.getBrightnessForRender(partialTicks);
		int j = i >> 16 & 65535;
		int k = i & 65535;
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		
		BufferBuilder buffer = Tessellator.getInstance().getBuffer();
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
		
		buffer.pos((double) (f5 - rotationX * f4 - rotationXY * f4), (double) (f6 - rotationZ * f4), (double) (f7 - rotationYZ * f4 - rotationXZ * f4)).tex((double) f, (double) f3).lightmap(j, k).color(this.particleRed, this.particleGreen, this.particleBlue, 1.0F).endVertex();
		buffer.pos((double) (f5 - rotationX * f4 + rotationXY * f4), (double) (f6 + rotationZ * f4), (double) (f7 - rotationYZ * f4 + rotationXZ * f4)).tex((double) f, (double) f2).lightmap(j, k).color(this.particleRed, this.particleGreen, this.particleBlue, 1.0F).endVertex();
		buffer.pos((double) (f5 + rotationX * f4 + rotationXY * f4), (double) (f6 + rotationZ * f4), (double) (f7 + rotationYZ * f4 + rotationXZ * f4)).tex((double) f1, (double) f2).lightmap(j, k).color(this.particleRed, this.particleGreen, this.particleBlue, 1.0F).endVertex();
		buffer.pos((double) (f5 + rotationX * f4 - rotationXY * f4), (double) (f6 - rotationZ * f4), (double) (f7 + rotationYZ * f4 - rotationXZ * f4)).tex((double) f1, (double) f3).lightmap(j, k).color(this.particleRed, this.particleGreen, this.particleBlue, 1.0F).endVertex();
		
		Tessellator.getInstance().draw();
	}
	
	@Override
	public int getBrightnessForRender(float partialTick)
	{
		int i = super.getBrightnessForRender(partialTick);
		int j = 0;
		if(this.world.isBlockLoaded(this.sourcePos))
			j = this.world.getCombinedLight(this.sourcePos, 0);
		return i == 0 ? j : i;
	}
}