package com.zeitheron.visuals.client.models;

import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelSingleChestInside extends ModelSimple<ResourceLocation>
{
	public ModelRenderer shape4;
	public ModelRenderer shape4_1;
	public ModelRenderer shape6;
	public ModelRenderer shape4_2;
	public ModelRenderer shape4_3;
	
	public ModelSingleChestInside()
	{
		super(64, 64, "");
		this.shape4_3 = new ModelRenderer(this, 15, 0);
		this.shape4_3.setRotationPoint(14.0F, 6.0F, 2.0F);
		this.shape4_3.addBox(0.0F, 0.0F, 0.0F, 12, 8, 1, 0.0F);
		this.setRotateAngle(shape4_3, 0.0F, -1.5707963267948966F, 0.0F);
		this.shape4_2 = new ModelRenderer(this, 15, 0);
		this.shape4_2.setRotationPoint(2.0F, 6.0F, 14.0F);
		this.shape4_2.addBox(0.0F, 0.0F, 0.0F, 12, 8, 1, 0.0F);
		this.setRotateAngle(shape4_2, 0.0F, 1.5707963267948966F, 0.0F);
		this.shape4 = new ModelRenderer(this, 15, 0);
		this.shape4.setRotationPoint(2.0F, 6.0F, 2.0F);
		this.shape4.addBox(0.0F, 0.0F, 0.0F, 12, 8, 1, 0.0F);
		this.shape4_1 = new ModelRenderer(this, 15, 0);
		this.shape4_1.setRotationPoint(14.0F, 6.0F, 14.0F);
		this.shape4_1.addBox(0.0F, 0.0F, 0.0F, 12, 8, 1, 0.0F);
		this.setRotateAngle(shape4_1, 0.0F, 3.141592653589793F, 0.0F);
		this.shape6 = new ModelRenderer(this, 17, 1);
		this.shape6.setRotationPoint(2.0F, 14.0F, 2.0F);
		this.shape6.addBox(0.0F, 0.0F, 0.0F, 12, 1, 12, 0.0F);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		this.shape4_3.render(f5);
		this.shape4_2.render(f5);
		this.shape4.render(f5);
		this.shape4_1.render(f5);
		this.shape6.render(f5);
	}
}