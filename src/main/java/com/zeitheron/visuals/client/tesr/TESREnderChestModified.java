package com.zeitheron.visuals.client.tesr;

import java.util.Iterator;
import java.util.Random;

import com.zeitheron.hammercore.client.render.shader.ShaderProgram;
import com.zeitheron.hammercore.client.render.shader.impl.ShaderEnderField;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.visuals.proxy.ClientProxy;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntityEnderChestRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityEnderChest;
import net.minecraft.util.NonNullList;

public class TESREnderChestModified extends TileEntityEnderChestRenderer
{
	public static int recursion = 0;
	
	@Override
	public void render(TileEntityEnderChest te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		++recursion;
		
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		
		float f = te.prevLidAngle + (te.lidAngle - te.prevLidAngle) * partialTicks;
		
		if(f > 0F)
		{
			GlStateManager.pushMatrix();
			GlStateManager.translate(x, y + .999F, z + 1);
			GlStateManager.rotate(180, 1, 0, 0);
			if(ShaderEnderField.useShaders())
			{
				if(ShaderEnderField.endShader == null)
					ShaderEnderField.reloadShader();
				if(ShaderEnderField.endShader != null)
				{
					ShaderEnderField.endShader.freeBindShader();
					UtilsFX.bindTexture("minecraft", "textures/entity/end_portal.png");
				}
			}
			TESRChestModified.CHEST_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
			if(ShaderEnderField.useShaders())
				ShaderProgram.unbindShader();
			
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			UtilsFX.bindTexture("visuals", "textures/entity/chest/ender.png");
			TESRChestModified.CHEST_INSIDE.shape4.render(0.06255F);
			TESRChestModified.CHEST_INSIDE.shape4_1.render(0.06245F);
			TESRChestModified.CHEST_INSIDE.shape4_2.render(0.06255F);
			TESRChestModified.CHEST_INSIDE.shape4_3.render(0.06245F);
			
			GlStateManager.popMatrix();
			
			NonNullList<ItemStack> list = ClientProxy.enderChestTop8Items;
			
			if(list != null && recursion < 4)
			{
				GlStateManager.pushMatrix();
				GlStateManager.translate(x + .5F, y - .6F, z + .5F);
				GlStateManager.enableCull();
				Random random = new Random();
				
				random.setSeed(254L);
				int shift = 0;
				float blockScale = .6F;
				float distMult = 1F;
				float timeD = (float) (360.0D * (double) (System.currentTimeMillis() & 16383L) / 16383.0D) - partialTicks;
				if(list.get(1).isEmpty())
				{
					shift = 8;
					blockScale = 0.85F;
				}
				
				GlStateManager.translate(-.5F, .5F, -.5F);
				TESRChestModified.customitem.setWorld(te.getWorld());
				TESRChestModified.customitem.hoverStart = 0;
				Iterator<ItemStack> var19 = list.iterator();
				
				while(var19.hasNext())
				{
					ItemStack item = var19.next();
					if(shift > TESRChestModified.shifts.length || shift > 8)
						break;
					
					if(item.isEmpty())
						++shift;
					else
					{
						float shiftX = TESRChestModified.shifts[shift][0] * distMult;
						float shiftY = TESRChestModified.shifts[shift][1] * distMult;
						float shiftZ = TESRChestModified.shifts[shift][2] * distMult;
						++shift;
						GlStateManager.pushMatrix();
						GlStateManager.translate(shiftX, shiftY, shiftZ);
						GlStateManager.rotate(timeD, 0.0F, 1.0F, 0.0F);
						GlStateManager.scale(blockScale, blockScale, blockScale);
						TESRChestModified.customitem.setItem(item);
						
						TESRChestModified.getRenderEntityItem().doRender(TESRChestModified.customitem, 0.0D, 0.0D, 0.0D, 0.0F, 0);
						GlStateManager.popMatrix();
					}
				}
				
				GlStateManager.popMatrix();
			}
		}
		
		--recursion;
	}
}