package com.zeitheron.visuals.client.wlar;

import com.zeitheron.visuals.api.client.WLAR;

import net.minecraft.block.BlockAnvil;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class WLARAnvil extends WLAR<TileEntity>
{
	@Override
	public void render(World world, BlockPos pos, IBlockState state, double x, double y, double z, float partialTime, NonNullList<ItemStack> items)
	{
		if(items.size() != 3)
			return;
		
		EnumFacing face = state.getValue(BlockAnvil.FACING);
		
		ItemStack in1 = items.get(0);
		ItemStack in2 = items.get(1);
		ItemStack out = items.get(2);
		
		RenderHelper.enableStandardItemLighting();
		
		RenderItem ri = Minecraft.getMinecraft().getRenderItem();
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(pos.getX() - TileEntityRendererDispatcher.staticPlayerX, pos.getY() - TileEntityRendererDispatcher.staticPlayerY - .15, pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ);
		
		if(face == EnumFacing.SOUTH)
			GlStateManager.translate(-1F, 0F, 0F);
		
		if(face == EnumFacing.WEST)
		{
			GlStateManager.rotate(90, 0, 1, 0);
			GlStateManager.translate(-1.5, 0, .5);
		}
		
		if(face == EnumFacing.EAST)
		{
			GlStateManager.rotate(-90, 0, 1, 0);
			GlStateManager.translate(-.5, 0, -1.5);
		}
		
		if(!in1.isEmpty())
		{
			GlStateManager.pushMatrix();
			GlStateManager.translate(1, .8, .75);
			GlStateManager.rotate(face.getHorizontalAngle() + 90, 0, 1, 0);
			
			GlStateManager.translate(.35F, 0F, .35F);
			GlStateManager.rotate(90, 1, 0, 0);
			GlStateManager.translate(-.35F, 0F, -.35F);
			
			ri.renderItem(in1, TransformType.GROUND);
			GlStateManager.popMatrix();
		}
		
		if(!in2.isEmpty())
		{
			GlStateManager.pushMatrix();
			GlStateManager.translate(1, .8, .275);
			GlStateManager.rotate(face.getHorizontalAngle() + 90, 0, 1, 0);
			
			GlStateManager.translate(.35F, 0F, .35F);
			GlStateManager.rotate(90, 1, 0, 0);
			GlStateManager.translate(-.35F, 0F, -.35F);
			
			ri.renderItem(in2, TransformType.GROUND);
			GlStateManager.popMatrix();
		}
		
		if(!out.isEmpty())
		{
			float timeDeg = (world.getTotalWorldTime() * 2) % 720F + partialTime * 2F;
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(1, .9 + Math.sin(Math.toRadians(timeDeg)) * .05F, .5);
			GlStateManager.rotate(face.getHorizontalAngle() + 90, 0, 1, 0);
			
			GlStateManager.translate(.35F, 0F, .35F);
			GlStateManager.rotate(90, 1, 0, 0);
			GlStateManager.translate(-.35F, 0F, -.35F);
			
			ri.renderItem(out, TransformType.GROUND);
			GlStateManager.popMatrix();
		}
		
		GlStateManager.popMatrix();
	}
}