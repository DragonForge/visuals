package com.zeitheron.visuals.client.wlar;

import javax.vecmath.Matrix4f;

import com.zeitheron.visuals.api.client.WLAR;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityEnchantmentTable;
import net.minecraft.util.NonNullList;

public class WLAREnchantmentTable extends WLAR<TileEntityEnchantmentTable>
{
	@Override
	public void render(TileEntityEnchantmentTable tile, double x, double y, double z, float partialTime, NonNullList<ItemStack> items)
	{
		if(items.size() != 2)
			return;
		
		ItemStack toEnch = items.get(0);
		ItemStack lapis = items.get(1);
		lapis.setCount(Math.min(3, lapis.getCount()));
		
		RenderHelper.enableStandardItemLighting();
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x, y - .15, z);
		
		RenderItem ri = Minecraft.getMinecraft().getRenderItem();
		
		float timeDeg = (tile.getWorld().getTotalWorldTime() * 2) % 720F + partialTime * 2F;
		
		if(!lapis.isEmpty())
		{
			int count = lapis.getCount();
			float perDeg = 360F / count;
			float curDeg = timeDeg;
			
			for(int j = 0; j < count; ++j)
			{
				float xOff = (float) Math.sin(Math.toRadians(curDeg));
				float yLev = (float) Math.sin(Math.toRadians(timeDeg + curDeg));
				float zOff = (float) Math.cos(Math.toRadians(curDeg));
				
				GlStateManager.pushMatrix();
				GlStateManager.translate(.5 + xOff * .35, 1.25 + yLev * .125, .5 + zOff * .35);
				GlStateManager.rotate(curDeg, 0, 1, 0);
				ri.renderItem(lapis, TransformType.GROUND);
				GlStateManager.popMatrix();
				
				curDeg += perDeg;
			}
		}
		
		if(!toEnch.isEmpty())
		{
			GlStateManager.pushMatrix();
			GlStateManager.translate(.5, 1.35, .5);
			GlStateManager.rotate(timeDeg / 2F, 0, 1, 0);
			
			IBakedModel model = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(toEnch);
			Matrix4f mat = model.handlePerspective(TransformType.THIRD_PERSON_RIGHT_HAND).getValue();
			
			if(mat.m20 != 0 && mat.m21 != 0 && mat.m22 != 0 && mat.m23 != 0)
			{
				GlStateManager.translate(0, .45F, 0);
				
				GlStateManager.translate(.35F, 0F, .35F);
				GlStateManager.rotate(45, 0, 0, 1);
				GlStateManager.translate(-.35F, 0F, -.35F);
			}
			
			ri.renderItem(toEnch, TransformType.GROUND);
			GlStateManager.popMatrix();
		}
		
		GlStateManager.popMatrix();
	}
}