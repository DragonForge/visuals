package com.zeitheron.visuals.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

public class CallableMap<K, V> implements Map<K, V>
{
	final Map<K, V> parent;
	final BiFunction<K, V, V> transformer;
	
	public CallableMap(Map<K, V> parent, BiFunction<K, V, V> transformer)
	{
		this.parent = parent;
		this.transformer = transformer;
	}
	
	@Override
	public void clear()
	{
		parent.clear();
	}
	
	@Override
	public boolean containsKey(Object key)
	{
		return parent.containsKey(key);
	}
	
	@Override
	public boolean containsValue(Object value)
	{
		return parent.containsValue(value);
	}
	
	@Override
	public Set<Entry<K, V>> entrySet()
	{
		return parent.entrySet();
	}
	
	@Override
	public V get(Object key)
	{
		return parent.get(key);
	}
	
	@Override
	public boolean isEmpty()
	{
		return parent.isEmpty();
	}
	
	@Override
	public Set<K> keySet()
	{
		return parent.keySet();
	}
	
	@Override
	public V put(K key, V value)
	{
		return parent.put(key, transformer.apply(key, value));
	}
	
	@Override
	public void putAll(Map<? extends K, ? extends V> m)
	{
		parent.putAll(m);
	}
	
	@Override
	public V remove(Object key)
	{
		return parent.remove(key);
	}
	
	@Override
	public int size()
	{
		return parent.size();
	}
	
	@Override
	public Collection<V> values()
	{
		return parent.values();
	}
}