package com.zeitheron.visuals.util;

import java.util.List;
import java.util.stream.Collectors;

import com.zeitheron.hammercore.internal.Chat.ChatFingerprint;
import com.zeitheron.hammercore.internal.Chat.FingerprintedChatLine;
import com.zeitheron.hammercore.net.internal.chat.PacketEditMessage;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ChatHelper
{
	@SideOnly(Side.CLIENT)
	public static void printNoSpam_client(long id, ITextComponent line)
	{
		ChatFingerprint fp = new ChatFingerprint(252535636L);
		
		List<FingerprintedChatLine> rem = FingerprintedChatLine.getChatLines().stream().filter(l -> l instanceof FingerprintedChatLine).map(l -> (FingerprintedChatLine) l).filter(l -> l.print.equals(fp)).collect(Collectors.toList());
		FingerprintedChatLine.getChatLines().removeAll(rem);
		Minecraft.getMinecraft().ingameGUI.getChatGUI().drawnChatLines.removeAll(rem);
		
		new PacketEditMessage().withPrint(fp).withText(line).executeOnClient(null);
	}
}