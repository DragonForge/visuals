package com.zeitheron.visuals.api;

import java.util.function.Consumer;

import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class ItemChangesTracker implements IContainerListener
{
	final int slot;
	final Consumer<Slot> update;
	final boolean doSendAll;
	
	public ItemChangesTracker(int slot, Consumer<Slot> update)
	{
		this.slot = slot;
		this.update = update;
		this.doSendAll = false;
	}
	
	public ItemChangesTracker(int slot, boolean doSendAll, Consumer<Slot> update)
	{
		this.slot = slot;
		this.update = update;
		this.doSendAll = doSendAll;
	}
	
	@Override
	public void sendAllContents(Container containerToSend, NonNullList<ItemStack> itemsList)
	{
		if(slot == -1)
			for(int i = 0; i < containerToSend.inventorySlots.size() && doSendAll; ++i)
				update.accept(containerToSend.getSlot(i));
		else
			update.accept(containerToSend.getSlot(slot));
	}
	
	@Override
	public void sendSlotContents(Container containerToSend, int slotInd, ItemStack stack)
	{
		if(slotInd == slot || slot == -1)
			update.accept(containerToSend.getSlot(slotInd));
	}
	
	@Override
	public void sendWindowProperty(Container containerIn, int varToUpdate, int newValue)
	{
	}
	
	@Override
	public void sendAllWindowProperties(Container containerIn, IInventory inventory)
	{
	}
}