package com.zeitheron.visuals.api;

import com.zeitheron.hammercore.utils.FastNoise;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

public class EarthPressure
{
	public static float getPressureValued(World world, Vec3d pos)
	{
		float wstr = MathHelper.sqrt(world.getRainStrength(1F));
		float randAdd = FastNoise.noise(pos.x / 8, pos.z / 8, 1) / 300F - .1F;
		int h = world.getSeaLevel();
		float heightVal = Math.max(0, (float) (pos.y < h ? h / (pos.y == 0 ? 0.00001 : pos.y) : (1 - pos.y / 275)));
		return heightVal + randAdd + wstr / 64F;
	}
	
	public static float getPressureValuei(World world, Vec3i pos)
	{
		float wstr = MathHelper.sqrt(world.getRainStrength(1F));
		float randAdd = FastNoise.noise(pos.getX() / 8F, pos.getZ() / 8F, 1) / 300F - .1F;
		float h = world.getSeaLevel();
		float heightVal = Math.max(0, (float) (pos.getY() < h ? h / (pos.getY() == 0 ? 0.00001 : pos.getY()) : (1 - pos.getY() / 275F)));
		return heightVal + randAdd + wstr / 64F;
	}
}