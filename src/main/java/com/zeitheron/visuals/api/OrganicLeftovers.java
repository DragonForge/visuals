package com.zeitheron.visuals.api;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class OrganicLeftovers
{
	public static final List<Block> REPLACEABLE = new ArrayList<>();
	
	static
	{
		add(Blocks.RED_FLOWER);
		add(Blocks.YELLOW_FLOWER);
		add(Blocks.TALLGRASS);
		add(Blocks.DOUBLE_PLANT);
	}
	
	public static void add(Block block)
	{
		if(!REPLACEABLE.contains(block))
			REPLACEABLE.add(block);
	}
}