package com.zeitheron.visuals.api;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;

public class FallingFix
{
	public static final Map<IBlockState, IBlockState> FALL_FIX = new HashMap<>();
	
	static
	{
		addFixFor(Blocks.SAND.getStateFromMeta(0), Blocks.SANDSTONE.getDefaultState());
		addFixFor(Blocks.SAND.getStateFromMeta(1), Blocks.RED_SANDSTONE.getDefaultState());
		addFixFor(Blocks.GRAVEL.getDefaultState(), Blocks.COBBLESTONE.getDefaultState());
	}
	
	public static void addFixFor(IBlockState falling, IBlockState notFalling)
	{
		FALL_FIX.put(falling, notFalling);
	}
	
	public static IBlockState getUnfallable(IBlockState falling)
	{
		return FALL_FIX.getOrDefault(falling, falling);
	}
}